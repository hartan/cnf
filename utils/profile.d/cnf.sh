# /etc/profile.d/cnf.sh - installs shell hooks to run "cnf" when a command
#                         wasn't found

# zsh weirdness
shell="$(readlink -f /proc/$$/exe)"
if [[ "$shell" =~ ".*zsh" ]]; then
    function command_not_found_handler {
        if command -v cnf 1>/dev/null; then
            cnf "$@"
        fi
    }
    return 0
fi

# All other shells
function command_not_found_handle {
    if command -v cnf 1>/dev/null; then
        cnf "$@"
    fi
}
return 0
