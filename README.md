# cnf - A distribution-agnostic "command not found"

The code in this repository is split into multiple distinct workspaces that
serve a different purpose each. If you're looking for the `cnf` terminal
application, head to the [main application workspace][cnf].


## [`cnf`][cnf] - The main TUI application

![cnf demo screenshot](src/cnf/assets/cnf_screenshot.png)

A TUI-application wrapping the functionality of [`cnf-lib`][cnf-lib]. This is
the projects main application.


## [`cnf-lib`][cnf-lib] - The library code

The core "library" code used by [`cnf`][cnf]. It manages the task of
translating command invocations between execution environments (currently the
host, [toolbx][toolbx] and [distrobox][distrobox]) and can be used by
third-party applications.


[toolbx]: https://containertoolbx.org/
[distrobox]: https://github.com/89luca89/distrobox
[cnf]: src/cnf
[cnf-lib]: src/cnf-lib
