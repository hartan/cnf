# Changelog

The changelog is kept per-workspace. Refer to the following changelogs instead:

- [`cnf` (TUI)](src/cnf/CHANGELOG.md)
- [`cnf-lib`](src/cnf-lib/CHANGELOG.md)

