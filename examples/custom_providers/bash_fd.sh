#!/usr/bin/env bash
#
# A custom provider written in bash, to demo the extensibility of `cnf`. It
# calls `fd` (must be installed) in each environment via `cnf`, presenting the
# results inside the `cnf` application. At the moment, it will search your
# whole `$HOME` folder (without external filesystems) for the given executable.
set -euo pipefail

# Marker for end of JSON payload
END_OF_MESSAGE=$'\007\n'
# Custom logfile
LOGFILE="/tmp/bash_fd_sh.log"
# Capture logflag from env
BASH_FD_DEBUG="${CNF_FD_DEBUG:-0}"

function _fatal {
    echo "FATAL  $@" 1>&2
    exit 1
}

function _log {
    DATE="$(date "+%Y-%m-%d %H:%M:%S")"
    echo "$DATE   $@" >> "$LOGFILE"
}

[[ "$#" -eq 1 ]] || _fatal "you must provide the command to search for"

REQUEST="{\"execute\": {
    \"cmd\": [
        \"fd\",
        \"--unrestricted\",
        \"--ignore-case\",
        \"--glob\",
        \"--absolute-path\",
        \"--print0\",
        \"--color\", \"never\",
        \"--type\", \"executable\",
        \"--one-file-system\",
        \"--search-path\", \"$HOME\",
        \"$1\"
    ]
}}"
_log "Sending request to CNF: $REQUEST"
echo "$REQUEST"; printf "$END_OF_MESSAGE"

# Wait for response
read -r -d $END_OF_MESSAGE RESPONSE
# The response here can be *huge*
[[ "$BASH_FD_DEBUG" -eq 1 ]] && _log "Received response: $RESPONSE" || \
    _log "Received response (Set 'BASH_FD_DEBUG=1' to see it here)"

STDOUT="$(jq '.["command-response"].stdout' <<< "$RESPONSE" | head -c-1 | tail -c+2)"
STDERR="$(jq '.["command-response"].stderr' <<< "$RESPONSE")"
EXIT_CODE="$(jq '.["command-response"].exit_code' <<< "$RESPONSE")"

if [[ "$EXIT_CODE" -ne 0 ]]; then
    _log "'fd' exited with errors, quitting..."
    _log "stderr: '$STDERR'"

    echo "{\"error\": \"fd terminated with non-zero exit code $EXIT_CODE: $STDERR\"}"
    printf "$END_OF_MESSAGE"
    exit 1
fi

function path2candidate {
    [[ "$#" -eq 1 ]] || _fatal "no candidate provided"
    PACKAGE="$(basename "$1")"
    ORIGIN="$(dirname "$1")"
    echo "    {"
    echo "        \"package\": \"$PACKAGE\","
    echo "        \"origin\": \"$ORIGIN\","
    echo "        \"actions\": {"
    echo "            \"execute\": {"
    echo "                \"cmd\": [ \"$1\" ]"
    echo "            }"
    echo "        }"
    echo "    }"
}

echo "{\"results\": ["
# Do some filtering of the results here
FIRST_ITERATION=0
while read -r -d '\0' CANDIDATE; do
    if [[ "$FIRST_ITERATION" -eq 0 ]]; then
        FIRST_ITERATION=1
    else
        # Terminate previous line
        echo ","
    fi
    # Strip weird leftovers from serializing NULL
    CANDIDATE="${CANDIDATE#"u0000"}"

    CANDIDATE_STRUCT="$(path2candidate "$CANDIDATE")"
    echo -n "$CANDIDATE_STRUCT"
    [[ "$BASH_FD_DEBUG" -eq 1 ]] && _log "ok '$CANDIDATE_STRUCT'" || \
        _log "ok '$CANDIDATE'"
done <<< "$STDOUT"

echo "    ]
}"
printf "$END_OF_MESSAGE"

