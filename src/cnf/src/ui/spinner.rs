// Copyright (C) 2023 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

//! Simple spinner for use with [`ratatui`].
use ratatui::style::{Modifier, Style};
use ratatui::text::{Span, Spans};
use ratatui::widgets::Paragraph;
use std::time::Instant;

/// A CLI spinner.
///
/// Built for use with [`ratatui`], updates the spinning values when printed repeatedly.
#[derive(Debug)]
pub struct Spinner {
    /// [`Instant`] when this spinner was instantiated.
    start: Instant,
}

impl Spinner {
    /// Create a new instance.
    pub fn new() -> Self {
        Spinner {
            start: Instant::now(),
        }
    }

    /// Turn spinner into a [`Paragraph`].
    ///
    /// The displayed text is customized for use with `cnf`.
    pub fn display(&self) -> Paragraph {
        let spinner = match self.start.elapsed().as_millis() % 1000 {
            0..=249 => "-",
            250..=499 => "/",
            500..=749 => "|",
            750..=999 => "\\",
            _ => unreachable!(),
        };
        let text = Spans::from(vec![
            Span::styled(spinner, Style::default().add_modifier(Modifier::DIM)),
            Span::raw(" "),
            Span::styled(
                "waiting for remaining results...",
                Style::default().add_modifier(Modifier::DIM | Modifier::ITALIC),
            ),
            Span::raw(" "),
            Span::styled(spinner, Style::default().add_modifier(Modifier::DIM)),
        ]);
        Paragraph::new(text)
            .alignment(ratatui::layout::Alignment::Center)
            .wrap(ratatui::widgets::Wrap { trim: true })
    }
}
