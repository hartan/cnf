# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
These are the categories specified by Keep a Changelog. Please pick a suitable
one and don't make up any for yourself.

### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security
-->

## [Unreleased]



## [0.5.2] - 2024-11-17

### Fixed
- Update broken `time` dependency (cnf!58)


## [0.5.1] - 2024-02-11

### Fixed
- Always start toolbx/distrobox containers in aliases (cnf!56)
- Relax recursion protection during alias execution (cnf!57)


## [0.5.0] - 2023-12-26

### Added
- Make documentation discoverable from `--help` output (cnf!45)
- Allow temporarily executing aliases with privileges (cnf!48)
- Detect and prevent infinite recursions (cnf!50)

### Changed
- Replace `log` with `tracing` (cnf!51)
- Replace `async-std` with `tokio` (cnf!52)

### Fixed
- Pass through exit codes of translated commands (cnf!43)
- Only start TUI when stdout is a TTY (cnf!55)


## [0.4.2] - 2023-08-24

### Fixed
- Fix quoting in shell hooks


## [0.4.1] - 2023-08-21

### Fixed
- Introduce 'lib'-subcrate to publish docs correctly (cnf!42)


## [0.4.0] - 2023-08-20

### Added
- Make errors in TUI expandable (cnf!19)
- User-configurable keybindings (cnf!28)
- Command aliases (cnf!29)
- Distrobox environment (cnf!31)
- Elevate privileged for command aliases (cnf!35)
- Improved, shell-based command aliases (cnf!39)

### Changed
- Split CLI input handling into separate module (cnf!26)
- Print error in shell hook if `cnf` isn't installed (cnf!32)
- Don't terminate on unknown env/provider in config (cnf!34)

### Fixed
- Unglitch terminal output during installation (cnf!23)
- Interactive authentication during installation (cnf!30)


## [0.3.0] - 2023-05-20

### Added
- Implement TUI
- Allow configuring log-level via config and CLI

### Changed
- Replace `crossterm` with `ratatui`
- Move UI to separate `ui` module
- Expand providers with single result in TUI tree by default
- Replace env-based logging to stdout with logfile
- Reporting on faulty config file

### Fixed
- TUI crash on unbound keys


## [0.2.0] - 2023-03-25

### Changed
- Cleanup `--hooks` output to pipe it into shell config directly

### Fixed
- Use correct config by filename
- Respect "toolbx" config setting


## [0.1.2] - 2023-03-20

### Added
- Logging to stdout controlled via env variables
- Print alternatives if multiple results exist

### Changed
- Propagate more expressive errors to users
- Automatically execute commands if only a single result exists


## [0.1.1] - 2022-02-01

### Added
- Read CLI args with `clap`


## [0.1.0] - 2022-01-31

### Added
- Configuration via config file
- Print error information to stderr

### Changed
- Rewrite the project in Rust from the original bash implementation


## Initial release - 2022-01-13

