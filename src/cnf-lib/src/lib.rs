// Copyright (C) 2023 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

#![doc = include_str!("../README.md")]
pub mod environment;
pub mod error;
pub mod provider;
#[cfg(any(test, doc))]
pub mod test;
pub mod util;

// Quicker access to modules
pub use environment as env;
pub use provider as prov;

pub use env::{Environment, Error as EnvironmentError, ExecutionError, IsEnvironment};
pub use prov::{
    search_in, Actions, Candidate, Error as ProviderError, IsProvider, Provider, Query,
};
pub use util::CommandLine;

pub mod prelude {
    pub use super::{
        search_in, CommandLine, Environment, EnvironmentError, IsEnvironment, IsProvider, Provider,
        ProviderError, Query,
    };
}
