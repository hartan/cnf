// Copyright (C) 2023 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

//! # Current Working Directory Provider
//!
//! Searches for commands matching the input in the current working directory. This provider runs
//! only in the current execution environment.
use crate::provider::prelude::*;
use crate::provider::CnfError;

use is_executable::is_executable;
use tokio::fs;

#[derive(Debug, PartialEq)]
pub struct Cwd {
    path: std::path::PathBuf,
}

impl Cwd {
    pub fn new() -> Result<Self, CnfError> {
        Ok(Cwd {
            path: std::env::current_dir().context("cannot create 'cwd' provider")?,
        })
    }

    pub fn with_path(path: std::path::PathBuf) -> Self {
        Cwd { path }
    }
}

impl fmt::Display for Cwd {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "cwd")
    }
}

#[async_trait]
impl IsProvider for Cwd {
    async fn search_internal(
        &self,
        command: &str,
        _target_env: Arc<crate::environment::Environment>,
    ) -> ProviderResult<Vec<Candidate>> {
        let mut results = vec![];

        if let Ok(mut diriter) = fs::read_dir(&self.path).await {
            while let Some(entry) = diriter.next_entry().await.transpose() {
                //for entry in diriter {
                if entry.is_err() {
                    continue;
                }
                let direntry = entry.unwrap();
                let entrypath = &direntry.path();
                trace!("checking file {}", entrypath.display());
                let metadata = match fs::metadata(entrypath).await {
                    Ok(val) => val,
                    Err(_) => continue,
                };

                if !metadata.file_type().is_file() {
                    trace!("skipping {}: Not a file", entrypath.display());
                    continue;
                }

                // It's a file, check whether the name somehow matches
                let filename = direntry.file_name();
                let filename = match filename.to_str().to_owned() {
                    Some(name) => name,
                    None => {
                        trace!("Skipping {}: Invalid unicode name", entrypath.display());
                        continue;
                    }
                };

                if filename.starts_with(command) {
                    // Match!
                    let pwd = std::env::current_dir().unwrap();
                    let filename = pwd.join(filename).display().to_string().to_owned();

                    let mut candidate = Candidate {
                        package: filename.clone(),
                        origin: pwd.display().to_string(),
                        actions: Actions {
                            install: None,
                            execute: cmd!(filename.clone()),
                        },
                        ..Candidate::default()
                    };

                    if !is_executable(entrypath) {
                        candidate.actions.install = Some(cmd!("chmod", "+x", &filename));
                    }
                    results.push(candidate);
                }
            }
            if !results.is_empty() {
                return Ok(results);
            }
        }
        Err(ProviderError::NotFound(command.to_string()))
    }
}
