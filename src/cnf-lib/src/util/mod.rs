// Copyright (C) 2023 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

//! Misc utilities for different purposes.
//!
//! This module houses a grab-bag of helpers and convenience-functions meant to make handling
//! different aspects of command finding easier.
pub mod command_line;
pub mod output_matcher;

pub use prelude::*;

pub mod prelude {
    pub use super::command_line::{cmd, CommandLine};
    pub use super::output_matcher::OutputMatcher;
}
