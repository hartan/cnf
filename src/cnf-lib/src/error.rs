// Copyright (C) 2023 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

//! # CNF Error Types
use thiserror::Error;

pub mod prelude {
    pub use super::CnfError;
    pub use anyhow::{anyhow, Context};

    /// Type alias for errors in this crate
    pub type Result<T> = std::result::Result<T, CnfError>;
}

#[derive(Error, Debug)]
pub enum CnfError {
    /// Transparent error from any source
    #[error(transparent)]
    ApplicationError(#[from] anyhow::Error),

    /// Required feature not implemented yet
    #[error("please implement '{0}' first!")]
    NotImplemented(String),
}
