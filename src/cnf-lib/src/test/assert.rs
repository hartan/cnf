// Copyright (C) 2023 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

//! Various asserts on [`Query`](crate::provider::Query) types.

/// Assert that a query turned up an error.
#[macro_export]
macro_rules! is_err {
    ($query:ident) => {
        assert!(
            $query.results.is_err(),
            "expected query to contain error: {:#?}",
            $query
        );
    };
}
pub use is_err;

pub mod err {
    //! Assertions for specific errors in tests.
    //!
    //! The errors are expected in the `results` member of a [`Query`](crate::provider::Query).

    /// Assert on [`crate::provider::Error::NotFound`].
    #[macro_export]
    macro_rules! not_found {
        ($query:ident) => {
            assert!(
                matches!($query.results, Err(ProviderError::NotFound(_))),
                "query didn't hold expected error 'NotFound': {:#?}",
                $query
            );
        };
    }
    pub use not_found;

    /// Assert on [`crate::provider::Error::Requirements`].
    #[macro_export]
    macro_rules! requirements {
        ($query:ident) => {
            assert!(
                matches!($query.results, Err(ProviderError::Requirements(_))),
                "query didn't hold expected error 'Requirements': {:#?}",
                $query
            );
        };
    }
    pub use requirements;

    /// Assert on [`crate::provider::Error::Execution`].
    #[macro_export]
    macro_rules! execution {
        ($query:ident) => {
            assert!(
                matches!($query.results, Err(ProviderError::Execution(_))),
                "query didn't hold expected error 'Execution': {:#?}",
                $query
            );
        };
    }
    pub use execution;

    /// Assert on [`crate::provider::Error::ApplicationError`].
    #[macro_export]
    macro_rules! application {
        ($query:ident) => {
            assert!(
                matches!($query.results, Err(ProviderError::ApplicationError(_))),
                "query didn't hold expected error 'ApplicationError': {:#?}",
                $query
            );
        };
    }
    pub use application;
}
