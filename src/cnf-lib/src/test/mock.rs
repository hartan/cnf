// Copyright (C) 2023 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

//! Mockups for unit testing.
//!
//! At the moment only contains a [`Mock` env](Mock) implementation, used to simulate command
//! executions in unit tests with [`output_of()`](crate::environment::Environment::output_of).
use std::cell::RefCell;
use std::collections::VecDeque;
use std::sync::Arc;
use std::sync::Mutex;

use crate::environment::prelude::*;
use crate::environment::ExecutionError;

/// Mock environment.
///
/// Only available when running tests, implements the [`IsEnvironment`][crate::env::IsEnvironment]
/// trait to allow testing [`Provider`](crate::provider::Provider) implementations. Holds an
/// internal queue of "replies" to return, using the [`Mock::push_raw()`] and [`Mock::pop_raw()`]
/// functions. Replies are returned from [`pop_raw()`](Mock::pop_raw) in the exact order they were
/// pushed in.
#[derive(Debug, Serialize, Deserialize)]
pub struct Mock {
    #[serde(skip)]
    queue: Mutex<RefCell<VecDeque<Result<String, ExecutionError>>>>,
}

// I don't care about these, but they're needed due to the trait requirements on `IsEnvironment`.
impl PartialEq for Mock {
    fn eq(&self, _other: &Self) -> bool {
        true
    }
}
impl Eq for Mock {}

// We don't really care how instances of `Mock` are sorted. This trait bound mainly originates from
// external use of environment instaces for purposes of ordering etc, but this is irrelevant for
// testing.
#[allow(clippy::non_canonical_partial_ord_impl)]
impl PartialOrd for Mock {
    fn partial_cmp(&self, _other: &Self) -> Option<std::cmp::Ordering> {
        Some(std::cmp::Ordering::Equal)
    }
}
impl Ord for Mock {
    fn cmp(&self, _other: &Self) -> std::cmp::Ordering {
        std::cmp::Ordering::Equal
    }
}

impl Default for Mock {
    fn default() -> Self {
        Self {
            queue: Mutex::new(RefCell::new(VecDeque::new())),
        }
    }
}

impl Mock {
    pub fn new() -> Self {
        Self::default()
    }

    /// Push a raw result into the result queue.
    pub fn push_raw(&self, entry: Result<String, ExecutionError>) {
        self.queue.lock().unwrap().borrow_mut().push_back(entry);
    }

    /// Pop a raw result from the result queue.
    pub fn pop_raw(&self) -> Result<String, ExecutionError> {
        self.queue
            .lock()
            .unwrap()
            .borrow_mut()
            .pop_front()
            .expect("requested more entries from mock env than were previously created")
    }

    /// Convert into an `Arc<Environment>`.
    pub fn to_env(self) -> Arc<Environment> {
        Arc::new(Environment::Mock(self))
    }
}

impl fmt::Display for Mock {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "MOCK (test-only)")
    }
}

#[async_trait]
impl environment::IsEnvironment for Mock {
    type Err = std::convert::Infallible;

    async fn exists(&self) -> bool {
        true
    }

    async fn execute(&self, _command: CommandLine) -> Result<Command, Self::Err> {
        // We just assume 'rustc' to be available on any system capable of running `cargo
        // test`.
        let mut cmd = Command::new("rustc");
        cmd.arg("--version");
        Ok(cmd)
    }
}
