// Copyright (C) 2023 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

//! Shared utilities for testing parts of `cnf`.
pub mod assert;
pub mod mock;

pub mod prelude {
    //! Essential includes for tests.
    pub use crate::environment::ExecutionError;
    pub use crate::provider::search_in;
    pub use crate::test::mock::Mock;
    #[cfg(unix)]
    pub use std::os::unix::process::ExitStatusExt;
    #[cfg(windows)]
    pub use std::os::windows::process::ExitStatusExt;
    pub use std::process::ExitStatus;
    pub use std::sync::Arc;

    pub use super::quick_test;
    pub use crate::test;
    pub use crate::test::assert;
}

/// Convenient generation of query results.
///
/// Takes an implementor of [`IsProvider`](crate::provider::IsProvider) as first argument and an
/// aribtrary amount of `Result<String, ExecutionError>` as further arguments. The second parameter
/// and all following are given as mock input to the `Provider` impl and are retrieved by
/// subsequent calls to e.g. [`Environment::output_of()`](crate::env::Environment::output_of()) in
/// the order specified.
///
/// The search term passed in the test is 'my-test-app', in case your application has to match
/// against it.
#[macro_export]
macro_rules! quick_test {
    ($provider:expr, $( $value:expr ),+) => {
        tokio::runtime::Runtime::new().unwrap().block_on(async {
            let provider = Arc::new(Provider::from($provider));
            let mock = Mock::new();
            $(
                mock.push_raw($value);
            )+

            search_in(provider, "my-test-app", mock.to_env()).await
        })
    };
}
pub use quick_test;

/// Insert a bunch of default tests.
///
/// - `ExecutionError::NotFound` should resolve to `ProviderError::Requirements`
#[macro_export]
macro_rules! default_tests {
    ($provider:expr) => {
        #[test]
        fn exec_not_found_is_requirements_error() {
            let query = $crate::test::quick_test!(
                $provider,
                Err(ExecutionError::NotFound("foo".to_string()))
            );

            assert::is_err!(query);
            assert::err::requirements!(query);
        }
    };
}
pub use default_tests;
