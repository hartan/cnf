// Copyright (C) 2023 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

//! # Host Environment Handler.
//!
//! This module handles command-not-found errors that occur while executing on the host OS.
//! Currently this means that the command is forwarded to a Toolbx container of choice, preferrably
//! one specified in the configuration file or the default one otherwise.
use super::prelude::*;

#[derive(PartialEq, Eq, Debug, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Host {}

impl fmt::Display for Host {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "host")
    }
}

impl Host {
    pub fn new() -> Host {
        Host {}
    }
}

impl std::default::Default for Host {
    fn default() -> Self {
        Self::new()
    }
}

#[async_trait]
impl environment::IsEnvironment for Host {
    type Err = std::convert::Infallible;

    async fn exists(&self) -> bool {
        true
    }

    async fn execute(&self, command: CommandLine) -> Result<Command, Self::Err> {
        debug!("preparing execution: {}", command);
        let mut cmd: Command;

        match environment::current() {
            Environment::Host(_) => {
                if command.get_privileged() {
                    if cfg!(unix) {
                        cmd = Command::new("sudo");
                        cmd.arg("-S");
                        if !command.get_interactive() {
                            cmd.arg("-n");
                        }
                    } else {
                        unimplemented!("cannot escalate privileges yet for platforms not Unix");
                    }

                    cmd.arg(command.command());
                } else {
                    cmd = Command::new(command.command());
                }

                cmd.args(command.args());
            }
            Environment::Toolbx(_) | Environment::Distrobox(_) => {
                // flatpak-spawn --host ARGS
                cmd = Command::new("flatpak-spawn");
                cmd.arg("--host");

                // Preserve environment
                for env in environment::read_env_vars() {
                    cmd.arg(format!("--env={}", env));
                }

                if command.get_privileged() {
                    if cfg!(unix) {
                        cmd.args(["sudo", "-S", "-E"]);
                        if !command.get_interactive() {
                            cmd.arg("-n");
                        }
                    } else {
                        unimplemented!("cannot escalate privileges yet for platforms not Unix");
                    }
                }

                cmd.arg(command.command());
                cmd.args(command.args());
            }
            #[cfg(test)]
            Environment::Mock(_) => {
                unimplemented!()
            }
        }

        trace!("full command: {:?}", cmd);
        Ok(cmd)
    }
}
