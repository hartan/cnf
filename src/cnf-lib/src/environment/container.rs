// Copyright (C) 2023 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::error::CnfError;
use std::path::Path;

const CONTAINER_ENV: &str = "/run/.containerenv";

pub fn detect() -> bool {
    Path::new(CONTAINER_ENV).exists()
}

pub fn run(_args: &[&str]) -> Result<(), CnfError> {
    Err(CnfError::NotImplemented("Environment Container".into()))
}
