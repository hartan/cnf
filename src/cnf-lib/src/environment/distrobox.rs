// Copyright (C) 2023 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

//! # Distrobox Environment Handler.
//!
//! [Distrobox][distrobox] is a collection of shell-scripts that serves the same purpose as
//! [toolbx][toolbx]: To provide pet-containers for e.g. development purposes
//!
//! [distrobox]: https://github.com/89luca89/distrobox
//! [toolbx]: https://containertoolbx.org/
use super::prelude::*;

use std::{io::IsTerminal, path::Path};

const DISTROBOX_ENV: &str = "/run/.containersetupdone";
const CONTAINER_ENV: &str = "/run/.containerenv";

#[derive(PartialEq, Eq, Debug, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Distrobox {
    name: String,
}

impl fmt::Display for Distrobox {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "distrobox '{}'", self.name)
    }
}

impl Distrobox {
    /// Start a Distrobox container with a given name.
    ///
    /// Checks if the distrobox container exists and starts it, if necessary. If `None` is given as
    /// name, will fall back to the default distrobox name and start that instead. Returns an error
    /// if unsuccessful.
    pub fn new(name: Option<String>) -> Result<Self, NewDistroboxError> {
        let name = match name {
            Some(name) if !name.is_empty() => name,
            _ => "my-distrobox".to_string(),
        };

        // Do an optimistic start:
        // - If the container exists and isn't started, it will be started
        // - If the container exists and is started, nothing happens
        // - If the container doesn't exist, we get an error and report that
        let ret = Self { name: name.clone() };
        ret.start()
            .map_err(|e| NewDistroboxError::CannotStart { source: e, name })?;
        Ok(ret)
    }

    /// Starts a given distrobox container.
    ///
    /// This function is automatically called by `new()` above and should only ever be called when
    /// creating a `Distrobox` object without using the constructor. This is currently the case
    /// when executing aliases in `cnf`, as the `Distrobox` instance is deserialized from the
    /// config in that case.
    pub fn start(&self) -> Result<(), StartDistroboxError> {
        let output = std::process::Command::new("podman")
            .args(["start", &self.name])
            .output()
            .map_err(|e| match e.kind() {
                std::io::ErrorKind::NotFound => StartDistroboxError::NeedPodman,
                _ => StartDistroboxError::IoError(e),
            })?;
        if output.status.success() {
            // All good
            Ok(())
        } else {
            let matcher = OutputMatcher::new(&output);
            if matcher.starts_with("Error: no container with name or ID")
                && matcher.contains("found: no such container")
            {
                Err(StartDistroboxError::NonExistent(self.name.clone()))
            } else {
                Err(StartDistroboxError::Podman(output))
            }
        }
    }

    /// Get the Toolbx container currently executing CNF.
    ///
    /// Will return an error if the current execution environment isn't Toolbx.
    pub fn current() -> Result<Self, CurrentDistroboxError> {
        if !detect() {
            return Err(CurrentDistroboxError::NotAToolbx);
        }

        let content = std::fs::read_to_string(CONTAINER_ENV).map_err(|e| {
            CurrentDistroboxError::Environment {
                env_file: CONTAINER_ENV.to_string(),
                source: e,
            }
        })?;
        let name = content
            .lines()
            .find(|line| line.contains("name=\""))
            .ok_or_else(|| CurrentDistroboxError::Name(CONTAINER_ENV.to_string()))?
            .trim_start_matches("name=\"")
            .trim_end_matches('"');

        Ok(Self {
            name: name.to_string(),
        })
    }
}

#[async_trait]
impl environment::IsEnvironment for Distrobox {
    type Err = Error;

    async fn exists(&self) -> bool {
        if detect() {
            true
        } else if let Environment::Host(host) = environment::current() {
            // The result in this case is indeed `Infallible`, but switching an `if-let` for an
            // `unwrap` is outright stupid IMO.
            #[allow(irrefutable_let_patterns)]
            if let Ok(mut cmd) = host
                .execute(crate::environment::cmd!("distrobox", "--version"))
                .await
            {
                cmd.stdout(std::process::Stdio::null())
                    .stderr(std::process::Stdio::null())
                    .status()
                    .await
                    .map(|status| status.success())
                    .unwrap_or(false)
            } else {
                false
            }
        } else {
            false
        }
    }

    async fn execute(&self, command: CommandLine) -> Result<Command, Self::Err> {
        debug!("preparing execution: {}", command);
        let mut cmd: Command;

        match environment::current() {
            Environment::Distrobox(t) => {
                if self == &t {
                    // This is the toolbx container we are currently running in
                    // We expect toolbx containers to *always* run a unix OS, or at least something
                    // that has `sudo`.
                    if command.get_privileged() {
                        cmd = Command::new("sudo");
                        if !command.get_interactive() {
                            cmd.arg("-n");
                        }

                        cmd.arg(command.command());
                    } else {
                        cmd = Command::new(command.command());
                    }

                    cmd.args(command.args());
                } else {
                    return Err(Error::Unimplemented(
                        "running in a distrobox from another distrobox".to_string(),
                    ));
                }
            }
            Environment::Toolbx(_) => {
                return Err(Error::Unimplemented(
                    "running in a distrobox from a toolbx".to_string(),
                ));
            }
            Environment::Host(_) => {
                cmd = Command::new("distrobox");
                cmd.args(["enter", "--name", &self.name]);

                // Only attach to the tty if we really have a tty, too
                // FIXME: Is this really the correct way to check?
                if std::io::stdout().is_terminal() && std::io::stdin().is_terminal() {
                    cmd.arg("-T");
                }
                cmd.arg("--");

                // This is the real command we're looking for (with arguments)
                if command.get_privileged() {
                    cmd.args(["sudo", "-S", "-E"]);
                }

                cmd.arg(command.command()).args(command.args());
            }
            #[cfg(test)]
            Environment::Mock(_) => unimplemented!(),
        }

        trace!("full command: {:?}", cmd);
        Ok(cmd)
    }
}

/// Detect if the current execution environment is a Toolbx container.
///
/// Checks for the presence of the `.toolboxenv` files.
pub fn detect() -> bool {
    Path::new(DISTROBOX_ENV).exists()
}

#[derive(Debug, ThisError)]
pub enum StartDistroboxError {
    #[error("working with distrobox containers requires the 'podman' executable")]
    NeedPodman,

    #[error("podman exited with non-zero code: {0:#?}")]
    Podman(std::process::Output),

    #[error("no distrobox with name {0} exists")]
    NonExistent(String),

    #[error("unknown I/O error occured")]
    IoError(#[from] std::io::Error),
}

#[derive(Debug, ThisError)]
pub enum NewDistroboxError {
    #[error("failed to determine default distrobox name")]
    UnknownDefault(#[from] DefaultToolbxError),

    #[error("failed to start distrobox container with name '{name}'")]
    CannotStart {
        source: StartDistroboxError,
        name: String,
    },
}

#[derive(Debug, ThisError)]
pub enum CurrentDistroboxError {
    #[error("cannot read toolbx info from environment file '{env_file}'")]
    Environment {
        env_file: String,
        source: std::io::Error,
    },

    #[error("program currently isn't run from a toolbx")]
    NotAToolbx,

    #[error("failed to read toolbx name from environment file '{0}'")]
    Name(String),
}

#[derive(Debug, ThisError)]
pub enum DefaultToolbxError {
    #[error("failed to read OS information from '{file}'")]
    UnknownOs {
        file: String,
        source: std::io::Error,
    },

    #[error("cannot determine OS ID from os-release info")]
    Id,

    #[error("cannot determine OS VERSION_ID from os-release info")]
    VersionId,
}

#[derive(Debug, ThisError)]
pub enum Error {
    #[error("cannot determine current working directory")]
    UnknownCwd(#[from] std::io::Error),

    #[error("not implemented: {0}")]
    Unimplemented(String),
}
