# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
These are the categories specified by Keep a Changelog. Please pick a suitable
one and don't make up any for yourself.

### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security
-->

## [Unreleased]



## [0.5.2] - 2024-11-17

### Fixed
- Update broken `time` dependency (cnf!58)


## [0.5.1] - 2024-02-11

### Added
- Allow explicitly starting environments (cnf!56)


## [0.5.0] - 2023-12-26

### Changed
- Replace `log` with `tracing` (cnf!51)
- Replace `async-std` with `tokio` (cnf!52)

### Fixed
- Pass through exit codes of translated commands (cnf!43)
- Correctly parse package names for DNF with `nmap` as input (cnf!44)
- Prevent accidental container detach with toolbx (cnf!46)

### Removed
- Unused error variants in `CnfError` (cnf!49)


## [0.4.2] - 2023-08-24

## [0.4.1] - 2023-08-21

## [0.4.0] - 2023-08-20

### Added
- Allow configuring environments and providers to use (cnf!21)
- Preserve more environment variables when translating between environments (cnf!33)
- Distrobox environment (cnf!31)
- Make environments (de-)serialize for use with command aliases (cnf!39)

### Changed
- Unify command-line handling with `CommandLine` (cnf!18)
- Move `execute_with_output` to `Environment::output_of` (cnf!24)
- Split code into multiple workspaces (cnf!25)

### Fixed
- Bump `log` to 0.4.18 to fix builds on nightly (cnf!20)


## [0.3.0] - 2023-05-20

### Added
- Provider for `cargo`
- Refine error types to show unmet requirements
- Provider for `pacman`
- Provider for `custom` (user-written scripts/extensions)
- Expose custom providers in config
- Example custom provider written in bash
- Gitlab CI
- Provider for `apt`
- CI-job for docs
- `env` pre-requirements check: Don't run in envs that don't exist
- Convenience function to collect output from command execution in envs
- Separate/better error types for `env` module
- Introduce `test` submodule and write basic tests for all providers (cnf!12)
- Implement `flatpak` provider (cnf!14)
- Handle caches in `apt` provider (cnf!17)

### Changed
- Dispatch provider tasks from new macro
- Update README contents
- Add trait bounds to `Env` and `Provider` trait for better message display
- Completely rewrite `Provider` trait to improve result/error handling
- Improve comments/docs across codebase
- Rewrite `Query` to associate errors with providers/environments
- Introduce `enum_dispatch` for `providers` module
- Restructure `Provider` trait to reduce code duplication/boilerplate code
- Wrap all provider objects into `Arc` and get rid of `Clone` impl
- Improve CI speeds with per-branch caching
- Restructure `lib` entrypoint
- Log ignored/non-fatal errors
- Disable CI cache and move to custom self-hosted runners
- Rewrite `env::Handler` with associated error type to get rid of `anyhow`
- Complete overhaul of `providers/path`
- Rewrite `Toolbx` env and autostart toolbx when instantiating (cnf!10)
- Improve error conversion robustness in `ExecutionError` (cnf!11)
- Improve error/cache handling in `dnf` provider (cnf!13)
- **BREAKING**: Rename core modules/traits/enums (cnf!15)

### Fixed
- Empty `execution` actions for results by `dnf`
- Accidental config duplication when instantiating `toolbx` envs

### Removed
- `dispatch_provider!` macro in favor of a vector-based implementation


## [0.2.0] - 2023-03-25

### Added
- Provider for `dnf`

### Changed
- Rewrite and simplify README
- `providers::Provider` API returns more suitable data types
- Update error types in `env/toolbx`


## [0.1.2] - 2023-03-20

### Added
- More diverse error types
- `Handler` trait to unify execution environments
- Module documentation
- Detect current execution environment
- Basic module for command providers
- Implement first provider, `cwd`
- Implement provider `$PATH`
- Introduce `async` to codebase

### Changed
- Prefer `podman exec` over `toolbox run` for performance reasons
- Propagate more expressive errors to users
- Refactor `env` module to collect all environments in an `env::Env` enum
- Overhaul `providers` trait API

### Removed
- Legacy code in `env::toolbx`


## [0.1.1] - 2022-02-01

> No changes

## [0.1.0] - 2022-01-31

> No changes

## Initial release - 2022-01-13

