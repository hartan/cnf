# cnf-lib - Library code for `cnf`

The code contained here serves three main purposes.


## Handling environments

An "environment" is anything that offers one or more "providers" (see below).
At the moment, regular Linux hosts, toolbx containers and distrobox containers
are supported environments, although this may change in the future.

It is often desirable to temporarily escape one environment for the sake of
executing a single command. There are usually controlled mechanisms to perform
this task. This crate aims to transparently manage the command translation
necessary in order to, for example, execute `htop` inside a toolbx container
while working on the host. All of this is implemented in the `Environment`
type.

Have a look at the documentation and code in `src/environment` if you want to
know more about this command translation.


## Handling providers

A "provider" is anything that offers executable software. This includes for
example the system `$PATH` (where executables are usually located and can be
executed immediately), but extends to regular package managers such as `dnf`,
usually requiring installation of commands before they can be executed.

Providers can be searched for a command and return a list of zero or more
possible candidates matching the search term. All of this is implemented in the
`Provider` type.


## Handling command invocations

Translating regular command lines between environments has a few pitfalls with
respect to e.g. privilege escalation and correctly reading stdout/stderr. The
`CommandLine` type provides the necessary abstractions to allow seamless
command execution without having to wonder how privilege escalation in the
target environment works, for example.
